# DevOps - Kubernetes Test

## Overview

In this brief technical test, we would like you to write a Kubernetes deployment
for a basic web application, following best practices.

## Setup

- Please use [k3d](https://k3d.io/) with the following command to provision a
  local Kubernetes cluster in Docker so that we can easily replicate your
  environment.

  This command will create a cluster called `local-test` and will expose the
  `web` entrypoint on your machine at port `8090`

  ```bash
  export K3D_CLUSTER=local-test
  export K8S_CLUSTER=k3d-${K3D_CLUSTER}

  k3d cluster create ${K3D_CLUSTER} \
    --servers 3 \
    -p "8090:80@loadbalancer"
  ```

- Verify you have access to the cluster and it is set as your active cluster

  ```
  $ k3d cluster list
  NAME           SERVERS   AGENTS   LOADBALANCER
  local-test     0/3       0/0      true

  $ kubectl config get-contexts
  CURRENT   NAME               CLUSTER            AUTHINFO                 NAMESPACE
  *         k3d-local-test     k3d-local-test     admin@k3d-local-test

  # You may have to wait a minute for all of the resources to be fully active
  $ kubectl -n kube-system get pods
  NAME                                      READY   STATUS      RESTARTS   AGE
  coredns-b96499967-wbrxb                   1/1     Running     0          82s
  helm-install-traefik-8dsrz                0/1     Completed   1          83s
  helm-install-traefik-crd-q5lz7            0/1     Completed   0          83s
  local-path-provisioner-7b7dc8d6f5-x7bpg   1/1     Running     0          82s
  metrics-server-668d979685-84zm8           1/1     Running     0          82s
  svclb-traefik-74e26939-8j98z              2/2     Running     0          32s
  svclb-traefik-74e26939-dhn49              2/2     Running     0          32s
  svclb-traefik-74e26939-hs4pt              2/2     Running     0          32s
  traefik-7cd4fcff68-4wvgt                  1/1     Running     0          32s
  ```

  - Hit your local machine on port `:8090` to make sure you can hit the Traefik
    ingress in the Kubernetes cluster

    ```bash
    $ curl localhost:8090
    404 page not found
    ```

## Criteria

1. Write Kubernetes manifest(s) to deploy a basic workload that runs an instance
   of NGINX (aka. 'the app') and any supporting infrastructure needed to expose it
   on your machine.

   - You should use the upstream `nginx` image.

     How you deploy this is up to you, but you should consider the following:

     - The workload is stateless
     - The app needs to be able to scale horizontally
     - The app needs to be fault-tolerant

   - You will need to deploy an instance in the `vp-app-prod` & `vp-app-dev` namespaces.

   - You can use the `resources/base.yaml` to provision the `Namespace`s &
     `IngressRoute`s required.

     This will map:

     - `http://localhost:8090/prod` -> prod instance
     - `http://localhost:8090/dev` -> dev instance

   - There should be 3 replicas of the application.

   - You will also need to deploy the `resources/configmap.yaml` to each namespace
     and then map parts of this `ConfigMap` into your NGINX instance:

     - `nginx.conf` -> `/etc/nginx/nginx.conf`
     - `index.html` -> `/usr/share/nginx/html/index.html`

   - The application should be evenly distributed across your Kubernetes nodes.

   - Once you have successfully deployed the application, you should be able to
     hit `http://localhost:8090/prod` (or `/dev`) and be presented with a web
     page that shows the current environment (`/prod` or `/dev`) and the current
     pod serving the page (This will change on a refresh).

   - You should follow any other Kubernetes best practices for these workloads.

2. Detail how you would deploy a new version to customers without any
   interruption to service.

3. Explain the next steps you would take when deploying these applications in a
   production environment.

## How to submit your test

- Fork this repo in Gitlab.com

- Add the group `https://gitlab.com/groups/victoriaplum-hiring/devops-test-reviewers`
  to your project so we can see your fork

- Add your Kubernetes manifests into the `submission` directory.

- Add your explanations in `submission/NOTES.md`

- Good luck!
