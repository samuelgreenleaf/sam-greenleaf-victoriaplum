#!/bin/bash

set -e

function print_usage {
    echo "Usage: $0 [-s] [-d environment]"
    echo "  -s: perform setup (create namespaces and apply base manifests)"
    echo "  -d: deploy to the specified environment (dev, prod, or all)"
}

# Define the namespaces
namespaces=("vp-app-prod" "vp-app-dev")

# Process command line options
while getopts "sd:" opt; do
  case ${opt} in
    s)
      setup=true
      ;;
    d)
      deploy=true
      environment=${OPTARG}
      if [ ${environment} == "all" ]; then
        echo "The deployment will be to all namespaces..."
      elif [ ${environment} == "dev" ]; then
        namespaces=("vp-app-dev")
        echo "DThe deployment will be to dev namespace..."
      elif [ ${environment} == "prod" ]; then
        namespaces=("vp-app-prod")
        echo "The deployment will be to prod namespace..."
      else
        echo "Invalid argument for -d. Please use 'dev', 'prod', or 'all'."
        exit 1
      fi
      ;;
    \?)
      echo "Invalid option: $OPTARG" 1>&2
      print_usage
      exit 1
      ;;
  esac
done
shift $((OPTIND -1))

# Check if either setup or deploy was specified
if [ -z "${setup}" ] && [ -z "${deploy}" ]; then
  echo "Either -s or -d must be specified."
  print_usage
  exit 1
fi

# Initial setup
if [ "${setup}" = true ]; then
    echo "--------------------------------------------"
    echo "Deploying base manifests "
    kubectl apply -f resources/base.yaml
  for ns in ${namespaces[@]}; do
      kubectl apply -f resources/configmap.yaml
  done
fi

# Deployment
if [ "${deploy}" = true ]; then
  for ns in ${namespaces[@]}; do
     echo "--------------------------------------------"
     echo "Starting deploy to $ns"
     kubectl apply -f deployment.yaml
     kubectl apply -f service.yaml
     kubectl apply -f hpa.yaml
     echo "Finished deploying to $ns"
     echo "--------------------------------------------"
  done
  echo "Deployment finished..."
fi
