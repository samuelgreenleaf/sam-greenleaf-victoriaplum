#!/bin/bash

set -e

echo "Installing K3D cluster..."
curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash >> logs.txt
echo "K3D installed."

echo "Configuring cluster..."
export K3D_CLUSTER=local-test
export K8S_CLUSTER=k3d-${K3D_CLUSTER}
if ! k3d cluster create ${K3D_CLUSTER} --servers 3 -p "8090:80@loadbalancer" &> logs.txt; then
    echo "Error creating cluster. Check logs.txt for details."
    exit 1
fi
echo "Cluster created."
