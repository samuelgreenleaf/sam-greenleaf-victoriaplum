# Deployment Script

This script facilitates the deployment of our application in our machine.

## Prerequisites

- You must have `kubectl` installed and configured to communicate with your Kubernetes cluster.
- You must have the `k3d` installed and the cluster configured. If you doesn't, please run the script `initial_setup.sh`. It will install and create de cluster.

## Usage

The local_deploy.sh script accepts two options:

- `-s` performs the initial setup. This command creates the namespaces and applies the base manifests.
- `-d` performs the deployment to a specific environment. This command accepts three values: `dev`, `prod`, and `all`.

### Usage Example

To perform the initial setup, you can run:

```bash
./local_deploy.sh -s
```

To deploy to the development environment, you can run:

```bash
./local_deploy.sh -d dev
```

To deploy to the production environment, you can run:

```bash
./local_deploy.sh -d prod
```

To deploy to all environments, you can run:

```bash
./local_deploy.sh -d all
```

Note that you can combine the options. For example, to perform the initial setup and then deploy to the development environment, you can run:

```bash
./local_deploy.sh -s -d dev
```

---
